import requests, json, yaml, pprint, os
pp = pprint.PrettyPrinter(indent=2)

#WES_URL="http://172.16.103.148:5000"
WES_URL="https://localhost"

# verify certificate
verify=False

# 1.) get service info
info = requests.get("{}/ga4gh/wes/v1/service-info".format(WES_URL), verify=verify)
pp.pprint(info.json())


# 2.) Next, we send a workflow to the WES server and monitor its processing. Here, the paths to genomics data in config file was updated with paths on WES server.
## load config file
with open("config.yaml") as file:
  workflow_params = json.dumps(yaml.load(file, Loader=yaml.FullLoader))

## create data object for request
data = {
  "workflow_params": workflow_params,
  "workflow_type": "SMK",
  "workflow_type_version": "6.10.0",
  "workflow_url": "Snakefile"
}

## attach workflow files
files = [
  ("workflow_attachment", ("Snakefile", open("Snakefile", "rb"))),
  ("workflow_attachment", ("bwa_samtools_bcftools.yaml", open("bwa_samtools_bcftools.yaml", "rb"))),
  ("workflow_attachment", ("py_plot.yaml", open("py_plot.yaml", "rb"))),
  ("workflow_attachment", ("plot-quals.py", open("plot-quals.py", "rb")))
]

## send request to server
response = requests.post("{}/ga4gh/wes/v1/runs".format(WES_URL), data=data, files=files, verify=verify)
response.json()


## get information about single run
results = requests.get("{}/ga4gh/wes/v1/runs/{}".format(WES_URL, response.json()["run_id"]), verify=verify)
pp.pprint(results.json())

# 3.) Finally, lets get all runs
info = requests.get("{}/ga4gh/wes/v1/runs".format(WES_URL), verify=verify)
pp.pprint(info.json())
