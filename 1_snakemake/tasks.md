# Initialization

Connect to virtual machine via Theia via browser

1. Clone repository

```bash
cd $HOME
git clone https://gitlab.com/twardzso/espace_eucancan_training_snakemake.git
```

2. Activate conda

```bash
conda init bash
source ~/.bashrc
```

# Snakemake

## Data

1. We download tutorial data from the official Snakemake repository - we highly recommend the complete tutorial for an advanced learning!

```bash
cd $HOME/espace_eucancan_training_snakemake/1_snakemake
wget https://github.com/snakemake/snakemake-tutorial-data/archive/v5.24.1.tar.gz
tar --wildcards -xf v5.24.1.tar.gz --strip 1 "*/data" "*/environment.yaml"
conda env create --file environment.yaml --name snakemake_training
conda activate snakemake_training
```

##  A simple workflow

1. The workflow step executes bwa mem at some input reads and transforms resulting alignment file into BAM format. Snakemake uses wildcards for generalization; here `{sample}` is used to generalize for input reads:

```python
configfile: "config.yaml"

def get_reads(wildcards):
    return config["samples"][wildcards.sample]

rule bwa_map:
    input:
        genome=config["genome"],
        reads=get_reads
    output:
        alignment="mapped_reads/{sample}.bam"
    shell:
        "bwa mem {input.genome} {input.reads} | samtools view -Sb - > {output.alignment}"
```

* The config file contains paths to reference data and the samples data.

```yaml
genome: "data/genome.fa"
samples:
  A: "data/samples/A.fastq"
  B: "data/samples/B.fastq"
  C: "data/samples/C.fastq"
```

2. We create a workflow file (name: Snakefile) and a configfile (name: config.yaml) and copy the code into it. Run the workflow with:

```bash
snakemake -n mapped_reads/A.bam mapped_reads/B.bam
```

3. We add more steps to the workflow; sort alignments using samtools:

```python
rule samtools_sort:
    input:
        bam="mapped_reads/{sample}.bam"
    output:
        bam="sorted_reads/{sample}.bam"
    shell:
        "samtools sort -T sorted_reads/{wildcards.sample} -O bam {input.bam} > {output.bam}"
```

4. and create an alignment index file:

```python
rule samtools_index:
    input:
        bam="sorted_reads/{sample}.bam"
    output:
        bai="sorted_reads/{sample}.bam.bai"
    shell:
        "samtools index {input.bam}"
```

5. Lets also call some small nucleotide variants:

```python
SAMPLES = config["samples"].keys()

rule bcftools_call:
    input:
        fa=config["genome"],
        bams=expand("sorted_reads/{sample}.bam", sample=SAMPLES),
        bais=expand("sorted_reads/{sample}.bam.bai", sample=SAMPLES)
    output:
        "calls/all.vcf"
    shell:
        "samtools mpileup -g -f {input.fa} {input.bams} | "
        "bcftools call -mv - > {output}"
```

6. And we apply a custom script to visualize the results:

```python
rule plot_quals:
    input:
        "calls/all.vcf"
    output:
        "plots/quals.svg"
    script:
        "plot-quals.py"
```

7. Therefore, we need to add the python script to "$HOME/espace_eucancan_training_snakemake/1_snakemake/plot-quals.py":

```python
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
from pysam import VariantFile

quals = [record.qual for record in VariantFile(snakemake.input[0])]
plt.hist(quals)

plt.savefig(snakemake.output[0])
```

8. It is good practice to add a initial all rule at the beginning at the workflow, which will be executed by default

```python
rule all:
    input:
        "plots/quals.svg"
```

9. Lets use Snakemake to visualize the execution DAG; files can be downloaded e.g using right-click in theia or via scp.

```
snakemake --dag all | dot -Tsvg > dag.svg
```

8. Run it all!

```
snakemake -n
snakemake --cores 1 all
```
